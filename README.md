# Installation
## Requirements
* Java jdk 11
* maven 
* docker optional
## Procedure
Build jar file using fallowing command: \
mvn clean package 

# Run the application
On Windows: \
java -jar target\callsign-1.0.0.jar \
On Linux and MacOs: \ 
java -jar target/callsign-1.0.0.jar

## See the image
Open http://localhost:8080 in your web browser

# Docker (optional)
## Installation
After finishing maven build execute following command to build the docker image: \
docker build -t dejanlozanovic/callsign .
## Running docker image
docker run -p 8080:8080 dejanlozanovic/callsign



