package com.dejanlozanovic.callsign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallsignApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallsignApplication.class, args);
	}


}
