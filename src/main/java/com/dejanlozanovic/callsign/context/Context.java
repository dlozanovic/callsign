package com.dejanlozanovic.callsign.context;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.Random;

@SpringBootConfiguration
public class Context {

    @Bean
    public HttpMessageConverter<BufferedImage> bufferedImageHttpMessageConverter() {
        return new BufferedImageHttpMessageConverter();
    }

    @Bean
    public BufferedImage logoImage() throws Exception {
        return ImageIO.read(getClass().getResourceAsStream("/0.png"));
    }

    @Bean
    public Random newRandom() {
        return new Random();
    }

}
