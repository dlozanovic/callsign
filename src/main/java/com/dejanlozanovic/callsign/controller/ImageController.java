package com.dejanlozanovic.callsign.controller;

import com.dejanlozanovic.callsign.service.LogoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.awt.image.BufferedImage;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class ImageController {

    private LogoService service;

    @Autowired
    ImageController(LogoService service) {
        this.service = service;
    }

    @GetMapping(path = "/", produces = "image/png")
    public BufferedImage generateImage() {
        return service.generateImage();
    }
}
