package com.dejanlozanovic.callsign.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.util.Random;

@Service
public class LogoService {

    private BufferedImage logo;
    private Random random;

    @Autowired
    public LogoService(BufferedImage logo, Random random) {
        this.logo = logo;
        this.random = random;
    }

    public BufferedImage generateImage() {
        return imageDistortion(logo, random);
    }

    BufferedImage cloneImage(BufferedImage source) {
        // dodge the bullet original image is indexed instead of rgbA:D
        var destination = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);
        var graphics = destination.getGraphics();
        graphics.drawImage(source, 0, 0, null);
        graphics.dispose();

        return destination;
    }

    BufferedImage imageDistortion(BufferedImage source, Random random) {
        var image = cloneImage(source);
        var data = image.getRGB(0, 0, image.getWidth() / 2, image.getHeight(), null, 0, image.getWidth() / 2);

        for (int i = 0; i < data.length; i++) {
            data[i] = pixelDistortion(data[i], random);
        }
        image.setRGB(0, 0, image.getWidth() / 2, image.getHeight(), data, 0, image.getWidth() / 2);
        return image;
    }

    int pixelDistortion(int colour, Random random) {
        var blue = colour & 0xFF;
        var green = (colour >> 8) & 0xFF;
        var red = (colour >> 16) & 0xFF;

        red = colourDistortion(red, random);
        green = colourDistortion(green, random);
        blue = colourDistortion(blue, random);

        return (red << 16) | (green << 8) | blue;
    }

    int colourDistortion(int colour, Random random) {
        return Math.max(0, colour - random.nextInt(4));
    }

}
