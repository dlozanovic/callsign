package com.dejanlozanovic.callsign;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(classes = CallsignApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void integrationTest() throws Exception {
        var response = restTemplate.getForEntity("http://localhost:"+port+"/" , byte[].class);

        assertEquals(200, response.getStatusCodeValue());
        assertEquals("image/png", response.getHeaders().get("Content-Type").get(0));
    }

}
