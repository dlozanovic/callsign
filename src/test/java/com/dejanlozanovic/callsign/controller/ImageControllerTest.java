package com.dejanlozanovic.callsign.controller;

import com.dejanlozanovic.callsign.service.LogoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.image.BufferedImage;

@ExtendWith(MockitoExtension.class)
public class ImageControllerTest {

    @Mock
    LogoService service;

    @Mock
    BufferedImage image;

    @Test
    void testImageController() {
        var controller = new ImageController(service);
        when(service.generateImage()).thenReturn(image);

        var controllerImage = controller.generateImage();

        assertEquals(image,controllerImage);

    }
}
