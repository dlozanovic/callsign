package com.dejanlozanovic.callsign.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.Random;


@ExtendWith(MockitoExtension.class)
public class LogoServiceTest {

    static BufferedImage image;

    @Mock
    Random random;

    @BeforeAll
    static void setUp() throws Exception{
        image = ImageIO.read(LogoServiceTest.class.getResourceAsStream("/0.png"));
    }

    @Test
    void testCloneImage() {
        var service = new LogoService(null,null);
        var cloned = service.cloneImage(image);

        assertEquals(BufferedImage.TYPE_INT_RGB,cloned.getType());
        assertEquals(image.getWidth(),cloned.getWidth());
        assertEquals(image.getHeight(),cloned.getHeight());

        var expectedData = image.getRGB(0,0,image.getWidth(),image.getHeight(),null,0,image.getWidth());
        var actualData = cloned.getRGB(0,0,cloned.getWidth(),cloned.getHeight(),null,0,cloned.getWidth());

        assertArrayEquals(expectedData,actualData);
    }

    @Test
    void testColourDistortionDarkenColourForMaximum3Numbers() {
        var service = new LogoService(null,null);
        when(random.nextInt(4)).thenReturn(2);

        assertEquals(8,service.colourDistortion(10,random));

        verify(random,times(1)).nextInt(4);
    }

    @Test
    void testColourDistortionNeverGoesBelowZero() {
        var service = new LogoService(null,null);
        when(random.nextInt(4)).thenReturn(2);

        assertEquals(0,service.colourDistortion(1,random));

        verify(random,times(1)).nextInt(4);
    }

    @Test
    void testPixelDistortionShouldDarkenEachColour() {
        var service = new LogoService(null,null);
        when(random.nextInt(4)).thenReturn(3,1,2);
        var input = 0xF9E9D9;
        var expected = 0xF6E8D7;
        var actual = service.pixelDistortion(input,random);
        assertEquals(expected, actual);

        verify(random,times(3)).nextInt(4);
    }

    @Test
    void testPixelDistortionShouldNotGoNegative() {
        var service = new LogoService(null,null);
        when(random.nextInt(4)).thenReturn(3,1,2);
        var input = 0;
        var expected = 0;
        var actual = service.pixelDistortion(input,random);
        assertEquals(expected, actual);

        verify(random,times(3)).nextInt(4);
    }

    @Test
    void testImageDistortionAlteredLeftHalfOfTheImage() {
        var RANDOM_VALUE = 2;
        var service = new LogoService(null,null);

        when(random.nextInt(4)).thenReturn(RANDOM_VALUE);
        var distortedImage = service.imageDistortion(image,random);
        var width = image.getWidth();
        var height = image.getHeight();

        assertEquals(width,distortedImage.getWidth());
        assertEquals(height,distortedImage.getHeight());
        verify(random,times(3*width*height/2)).nextInt(4);

        var rightOriginal = image.getRGB(width/2,0,width/2,height,null,0,width/2);
        var rightDistorted = distortedImage.getRGB(width/2,0,width/2,height,null,0,width/2);

        assertArrayEquals(rightOriginal,rightDistorted);
    }

    @Test
    void testGenerateImageThatIsCallingimageDistortion() {
        var service = new LogoService(image,random);
        when(random.nextInt(4)).thenReturn(2);

        var distortedImage = service.generateImage();
        var width = image.getWidth();
        var height = image.getHeight();

        assertEquals(width,distortedImage.getWidth());
        assertEquals(height,distortedImage.getHeight());

        var rightOriginal = image.getRGB(width/2,0,width/2,height,null,0,width/2);
        var rightDistorted = distortedImage.getRGB(width/2,0,width/2,height,null,0,width/2);

        assertArrayEquals(rightOriginal,rightDistorted);


        var allDatatOriginal = image.getRGB(0,0,width/2,height,null,0,width);
        var allDataDistorted = distortedImage.getRGB(0,0,width/2,height,null,0,width);

        // make sure that at least one pixel have different pixel
        for(int i=0;i<allDataDistorted.length;i++) {
            if(allDataDistorted[i]!=allDatatOriginal[i]){
                return;
            }
        }
        fail();
    }
}
